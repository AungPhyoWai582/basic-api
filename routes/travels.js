const express = require('express');
const {
	getTravels,
	getTravel,
	createTravel,
	updateTravel,
	deleteTravel
} = require('../controllers/travels');
const { protect, authorize } = require('../middlewares/auth');

const router = express.Router();

router
	.route('/')
	.get(getTravels)
	.post(protect, authorize('admin', 'publisher'), createTravel);
router
	.route('/:id')
	.get(protect, getTravel)
	.put(protect, authorize('admin', 'publisher'), updateTravel)
	.delete(protect, authorize('admin', 'publisher'), deleteTravel);

module.exports = router;
