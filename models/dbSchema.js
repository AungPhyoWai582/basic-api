const mongoose = require('mongoose');
const TravelSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Please add a name'],
		maxlength: [50, 'Name can not be more than 50 characters'],
	},
	nrc: {
		type: String,
		required: [true, 'Please add a nrc'],
		unique: true,
		trim: true,
		maxlength: [20, 'Nrc can not be more than 20 characters'],
	},
	from: {
		type: String,
		required: true,
	},
	to: {
		type: String,
		required: true,
	},
	with: {
		type: String,
		required: true,
	},
	date: {
		type: Date,
		default: Date.now,
	},
});

//Create bootcamp slug from the name

module.exports = mongoose.model('Travels', TravelSchema);
