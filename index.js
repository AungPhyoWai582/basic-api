const express = require('express');
const bodyparser = require('body-parser');
const dotenv = require('dotenv');
const cookieParser = require('cookie-parser');
const app = express();
const travels = require('./routes/travels');
const auth = require('./routes/auth');
const errorHandler = require('./middlewares/errors');
const connectDB = require('./config/db');
const cors = require('cors');

//Load env vars
dotenv.config({ path: './config/config.env' });

//connect DB
connectDB();

app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());
app.use(express.json());

//Cookie parser
app.use(cookieParser());

//CORS
app.use(cors());
//middleware
app.use('/api/records', travels);
app.use('/api/auth', auth);
app.use(errorHandler);

//Server Starting
app.listen(3000, () => console.log('Server is running ... '));

//Handle unhandled promise rejections
process.on('unhandledRejection', (err, promise) => {
	console.log(`Error: ${err.message}`);
	//Close server and exit process
	server.close(() => process.exit(1));
});
