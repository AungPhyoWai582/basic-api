const Travels = require('../models/dbSchema');
const asyncHandler = require('../middlewares/asyncHandler');
const ErrorResponse = require('../utils/errorResponse');
//Get all Travels
exports.getTravels = asyncHandler(async (req, res) => {
	console.log(req.user);
	const travels = await Travels.find();
	res.status(200).json({
		meta: { success: true, count: travels.length },
		data: travels,
		link: {
			self: req.originalUrl,
		},
	});
});

//Get Travel with id
exports.getTravel = asyncHandler(async (req, res, next) => {
	const _id = req.params.id;
	const travel = await Travels.findById(_id);
	if (!travel) {
		return next(new ErrorResponse(`Travel not found with id of ${_id}`, 404));
	}
	res.status(200).json({
		meta: { success: true },
		data: travel,
		link: {
			self: req.originalUrl,
		},
	});
});

//Create Travel
exports.createTravel = asyncHandler(async (req, res) => {
	const travel = await Travels.create(req.body);
	res.status(201).json({
		meta: { success: true },
		data: travel,
		link: {
			self: req.originalUrl,
		},
	});
});

//Update Travel with id
exports.updateTravel = asyncHandler(async (req, res, next) => {
	const _id = req.params.id;
	const body = req.body;
	const travel = await Travels.findByIdAndUpdate(_id, body, {
		new: true,
		runValidators: true,
	});
	if (!travel) {
		return next(new ErrorResponse(`Travel not found with id of ${_id}`, 404));
	}
	res.status(200).json({
		meta: { success: true },
		data: travel,
		link: {
			self: req.originalUrl,
		},
	});
});

//Delete Travel
exports.deleteTravel = asyncHandler(async (req, res, next) => {
	const _id = req.params.id;
	const travel = await Travels.findByIdAndDelete(_id);
	if (!travel) {
		return next(new ErrorResponse(`Travel not found with id of ${_id}`, 404));
	}
	res.status(200).json({
		meta: {
			success: true,
		},
		data: {},
		link: {
			self: req.originalUrl,
		},
	});
});
